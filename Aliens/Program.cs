﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace Aliens
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamReader reader = new StreamReader("input.txt");
            int r1 = int.Parse(reader.ReadLine());
            int r2 = int.Parse(reader.ReadLine());
            int r3 = int.Parse(reader.ReadLine());
            reader.Close();

            int result;
            result = r2 + r3;

            if (result>r1)
            {
                StreamWriter writer = new StreamWriter("output.txt");
                writer.WriteLine($"NO");
                writer.Close();
            }

            if (result <= r1)
            {
                StreamWriter writer = new StreamWriter("output.txt");
                writer.WriteLine($"YES");
                writer.Close();
            }


            
        }
    }
}
